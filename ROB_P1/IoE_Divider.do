
# IoE_Divider.do

#vlib work
# vlog +acc  "IoE_Divider.v"  # Do this manually
# vlog +acc  "tb_IoE_Divider.v" # Do this manually

vsim -novopt -t 1ps -lib work IoE_Divider_tb
do {IoE_Divider_wave.do}
view wave
view structure
view signals
log -r *
run 4us
WaveRestoreZoom {0 ps} {500 ns}
