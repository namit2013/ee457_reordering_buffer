// EE457 RTL Exercises
// File Name: IoE_Divider.v 
// Written by Mingyu Chang; Instructed by Byeongju Cha and Gandhi Puvvada 
// Modified the OoO_Divider.v to convert it to IoE_Divider.v -- Gandhi 3/22/2015
// Implementation improvement and exercise assignment -- Sanmukh Kuppannagari 4/12/2015


// Description: This is an "in-order execution & in-order completion" divider. 
//              It reads a dividend-divisor pair from the memory array, mem_dividend_divisor, assigns the division to the next divider.
//              When the division is done, it collects the results (quotient-remainder pair) and writes it to the memory array, mem_quotient_remainder.
//				There are 32 divisions and the two arrays are 32-locations deep.
// 				Since there are 4 dividers, the following two unit are *separated*.
//				(a) the "assign" unit (assigning division jobs one by one to the dividers)
//  			(b) the "collection" unit (collecting results from the dividers one by one and depositing in the memory)
//				Both go in the simple order: single_divider divider0, single_divider divider1, single_divider divider2, single_divider divider3
// 				It is a requirement that you deposit the results (quotient-remainder pairs) in the same order 
// 				that you drew the operands (dividend-divisor pairs) i.e in the order of 0 to 31.
//				In this simple in-order execution unit we will be wasting opportunities to do more divisions in shorter time
//				as some of the dividers are done but we do not assign to them a new division in an out-of-order fashion.
//				In the next part, we overcome this by using a ROB.
//

`timescale 1 ns / 100 ps

module In_Order_divider (clk, rst,start, memory_write_complete);
input clk, rst, start;
output memory_write_complete;

//signals related to memory array
reg [15:0] mem_dividend_divisor [0:31];				// dividend-divisor pairs to be calculated initialized by testbench
reg [15:0] mem_quotient_remainder [0:31];			//quotient-remainder pairs calculated. 
// reg [7:0] mem_clocks_of_single_div_completion [0:31];//the clock number at which the division execution is complete, this is for displaying purpose. 
reg [7:0] mem_clocks_of_IoE_graduation [0:31];		//the clock number at which the IoE entry is graduated, this is for displaying purpose. 
reg [4:0] mem_dd_pointer, mem_qr_pointer; 			//pointers. we read from mem_dividend_divisor, write into mem_quotient_remainder. 
reg memory_read_complete;
wire memory_write_complete;

//signals related to single divider
reg [3:0] ack_from_IoE_div, start_from_IoE_div;		//output to single dividers
wire [2:0] single_div_states [0:3]; 				//input from single dividers
wire [7:0] dividend_of_IoE_div;						//output to single dividers
wire [7:0] divisor_of_IoE_div;						//output to single dividers
wire [7:0] quotient_of_IoE_div [0:3];				//input from single dividers
wire [7:0] remainder_of_IoE_div [0:3];				//input from single dividers
wire next_divider_not_available; // Please notice that here it is "next_divider_not_available" where as in the next part of this lab it is "no_divider_available"
wire next_divider_ready_for_collection; // 

reg [1:0] assign_pointer, collection_pointer; // these 2-bit pointers are actually 2-bit counters incremented after assigning and incremented after collecting

wire stall; // If next divider is not available, we need to stall assigning division tasks
reg [7:0] clock_counter;//keep counting clocks, this is for displaying purpose.

integer i,j;

reg [2:0] top_state; // This is the state of the top unit called next_divider_not_available
localparam

//states of IoE divider
Initial  = 3'b001,
Execution = 3'b010,
Complete = 3'b100,


//states of single divider 
// Please sure that you distinguish the above three constants and labels for the three states of the top-design
// from the three constants and labels below for the single divider
Qi = 3'b001,
Qc = 3'b010,
Qd = 3'b100;


// instantiation of single divider module
// Note: The "tag_in" and the "tag_out" pins are used in the next part. They are left unconnected in this part
single_divider divider0 (.Xin(dividend_of_IoE_div), .Yin(divisor_of_IoE_div), .Start(start_from_IoE_div[0]),.Ack(ack_from_IoE_div[0]),.Clk(clk),.Reset(rst),.Quotient(quotient_of_IoE_div[0]), .Remainder(remainder_of_IoE_div[0]), .tag_in(), .tag_out(), .state(single_div_states[0]));
single_divider divider1 (.Xin(dividend_of_IoE_div), .Yin(divisor_of_IoE_div), .Start(start_from_IoE_div[1]),.Ack(ack_from_IoE_div[1]),.Clk(clk),.Reset(rst),.Quotient(quotient_of_IoE_div[1]), .Remainder(remainder_of_IoE_div[1]), .tag_in(), .tag_out(), .state(single_div_states[1]));
single_divider divider2 (.Xin(dividend_of_IoE_div), .Yin(divisor_of_IoE_div), .Start(start_from_IoE_div[2]),.Ack(ack_from_IoE_div[2]),.Clk(clk),.Reset(rst),.Quotient(quotient_of_IoE_div[2]), .Remainder(remainder_of_IoE_div[2]), .tag_in(), .tag_out(), .state(single_div_states[2]));
single_divider divider3 (.Xin(dividend_of_IoE_div), .Yin(divisor_of_IoE_div), .Start(start_from_IoE_div[3]),.Ack(ack_from_IoE_div[3]),.Clk(clk),.Reset(rst),.Quotient(quotient_of_IoE_div[3]), .Remainder(remainder_of_IoE_div[3]), .tag_in(), .tag_out(), .state(single_div_states[3]));

//Task #1: next_divider_not_available
//Hint: Remember round robin.
//How do you find out whether the next divider to be assigned is in the correct state or not to be assigned.  

//check whether the divider corresponding to the assign_pointer is ready to be assigned or not
assign next_divider_not_available = ~( single_div_states[assign_pointer] ==  Initial );
assign stall = next_divider_not_available;// This is rather unnecessary name change from next_divider_not_available to stall here. But we left it here as we will add "rob_full" to this line in the next design.

//Task #2: next_divider_ready_for_collection
//Hint: How do you find out whether the next divider to be collected is in the correct state or not. 
//Check whether the divider corresponding to the collection_pointer is ready for collection or not.
assign next_divider_ready_for_collection = (  single_div_states[collection_pointer] == Complete );

assign memory_write_complete = (top_state == Complete);

//The following wires are assigned the next undispatched element from the input memory.  
assign	dividend_of_IoE_div = mem_dividend_divisor[mem_dd_pointer][15:8];//assign dividend from memory to single divider
assign	divisor_of_IoE_div  = mem_dividend_divisor[mem_dd_pointer][7:0];//assign divisor from memory to single divider

always @(posedge clk, posedge rst)
begin
if (rst) 
     begin
	     top_state <= Initial;
	     mem_dd_pointer <= 5'bxxxxx; // to avoid recirculating mux controlled by Reset
         mem_qr_pointer <= 5'bxxxxx;
         assign_pointer  <= 2'bxx;
         collection_pointer <= 2'bxx;
	     memory_read_complete <= 1'bx;
		 clock_counter <= 16'hxx;
     end
else
  begin	
 
    case(top_state)
      Initial:
        begin
	     mem_dd_pointer <= 5'b00000;
         mem_qr_pointer <= 5'b00000;
         assign_pointer  <= 2'b00;
         collection_pointer <= 2'b00;
	     memory_read_complete <= 1'b0;
	     if (start)
	        top_state <= Execution;
		    clock_counter <= 16'h01;
	    end   
	  	   
      Execution:	   
		begin   
		clock_counter <= clock_counter + 1; 
//		assigning unit assigns the division task to the next single divider pointed to by the assign_pointer 
//----------------------------------------------------------------------------------------
//Task #3: Fill the if condition.
//Hint: The contents of the if block means that a dividend divisor pair from the memory is read.
//Under what conditions will you read a dividend divisor pair and assign it to a single divider.
		if (  ~stall && ~memory_read_complete && (top_state == Execution) )
            begin 			 
				assign_pointer  <= assign_pointer + 1;	// increment assign_pointer.
				mem_dd_pointer <= mem_dd_pointer + 1;	//increment memory read pointer.
				if (mem_dd_pointer == 5'b11111)
					memory_read_complete <= 1'b1; 		//if this is the last entry, set memory_read_complete so that dispatching stops.
     		end
			
//----------------------------------------------------------------------------------------				
// 		collection unit's act!
//Task #4: Fill in the right hand side of the two statements in the if block
//Hint: find out where the quotient remainder pair for the divider to be collected is stored
	    if (next_divider_ready_for_collection == 1'b1)
          begin		
		  collection_pointer <= collection_pointer + 1;
		  mem_quotient_remainder[mem_qr_pointer][15:8] <= quotient_of_IoE_div[collection_pointer];
		  mem_quotient_remainder[mem_qr_pointer][7:0] <= remainder_of_IoE_div[collection_pointer];
		  mem_clocks_of_IoE_graduation[mem_qr_pointer] <= clock_counter;
		 mem_qr_pointer <= mem_qr_pointer + 1;// increment the memory write pointer.
		 end
		
		if (mem_qr_pointer == 5'b11111)
			top_state <= Complete;//if it is the last item, goes to complete top_state.                	        
		end
	
      Complete:
		begin
			top_state <= Initial;
		end
    endcase	
		
   end	      
end



// we separate the following block from the clocked block above, because we do not want to infer any registers for signals like 'start' and 'ack'.

always@(*)

begin
//assign unit. ----------------------------------------------------------------------------------------	
		start_from_IoE_div = 4'b0000;
        if ((stall == 1'b0)&&(memory_read_complete == 1'b0)&&(top_state == Execution))
            begin 			
			    if (single_div_states[assign_pointer] == Qi)
				begin
					start_from_IoE_div[assign_pointer] = 1'b1;//set that divider to start, notice that this "start" signals only last for one clock.
				end
					
            end	
//Collection unit.----------------------------------------------------------------------------------------				
	  ack_from_IoE_div = 4'b0000;
          begin
              if (single_div_states[collection_pointer] == Qd)
                 begin
			        ack_from_IoE_div[collection_pointer] = 1'b1; // acknowledge to single divider so that it reset its ready bit.
			     end
		  end
end

endmodule
