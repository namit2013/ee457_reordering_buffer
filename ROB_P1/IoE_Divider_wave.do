onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/Clk_tb
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/Reset_tb
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/Start_tb
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/complete_tb
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/single_div_states
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/stall
add wave -noupdate -format Literal /IoE_Divider_tb/IoE_DIV/top_state
add wave -noupdate -format Literal -radix ascii /IoE_Divider_tb/state_string
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/next_divider_not_available
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/next_divider_ready_for_collection
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/ack_from_IoE_div
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/start_from_IoE_div
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/assign_pointer
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/collection_pointer
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/mem_qr_pointer
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/mem_dd_pointer
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/memory_read_complete
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/memory_write_complete
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/dividend_of_IoE_div
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/divisor_of_IoE_div
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/quotient_of_IoE_div
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/remainder_of_IoE_div
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/mem_dividend_divisor
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/mem_quotient_remainder
add wave -noupdate -format Logic -radix hexadecimal /IoE_Divider_tb/IoE_DIV/mem_clocks_of_IoE_graduation

#TreeUpdate [SetDefaultTree]
#WaveRestoreCursors {{Cursor 1} {21910000 ps} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {500 ns}
